__Lesson 2 - Git, Bash, Python__
================================

Today we are going to work to get more familiar with git, bash and python.
Git is a version control software that, as you can see, has the ability to do so much more.
We will use git to organize future lessons and share and develop scripts. 
Bash is one of the scripting languages used in Linux and will come in very handy when working with your remote server.
Python is our powerful interperted scripting language. We will use python with both git and bash to accomplish our goals.

# Git

Git is an amazing version control repository. I asked you all to join the [BitBucket](https://bitbucket.org) in order to share our information. Think of this as a share drive on steriods. We can share text documents, upload files and all the while keep track of every change that takes place.
Git is amazingly powerful and will take some time to learn but we will start with the basics here and let you research more on your own. 

Learn git using this [tutorial](https://try.github.io)

Also you can learn bash (or hacking) at [Over the Wire](www.overthewire.org)

Git has local files you work on and can change as you see fit. Git can also push back up to a remote repository where you see we all can work on collaberating. 
If you make a change you need to let git know. First let set up this repository on your local machine.

## Setup Data_Analytics on your server

*Log into your server  
Recall the user@IP_ADDRESS and password from our last lesson
Go into your terminal (Linux/OSx) or PuTTy (Windows) using the command:  
`ssh user@IP_ADDRESS`  
You will be prompted for your password, enter it.  

Once on your server install Git:  
`sudo apt-get install git`  
Type you password and you will see it download the Git client software.  

You can read the manual page by typing:  
`man git`

Here you will find a wealth of information. [This Link](https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup) is where you can go for more info on initial setup.
For now lets just set up a few global parameters:

Tell Git, and thus the Git teams you are working on, your name
`git config --global user.name "John Doe"`
Replace John Doe with your name.

Give your team a way to contact you, an email. 
`git config --global user.email johndoe@example.com`

Replace johndoe@example.com with your email.

Lastly unless you are good at editing in vi, we better switch to nano to make our life easy.

`git config --global core.editor nano`

You can check your settings using,
`git config --list`


### Clone our Repo

Now that Git is set up lets simply make a directory to store our repository.

`mkdir bitbucket`

Now lets change directory into it

`cd bitbucket`

Once in there lets clone the data_analytics repo:

*NOTE: You will need to change the constantibe aspect of the URL to your user name*

`git clone https://Constantibe@bitbucket.org/macris/data_analytics.git`


# Bash Commands 

    `ls` list the contents of the directory
    `cd` change directory
    `git clone`
    `man` access the manual page for any command
    `ls /bin/` the directory of all system binaries
