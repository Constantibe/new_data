# **Data Management**

[http://nyctaxi.herokuapp.com/](http://nyctaxi.herokuapp.com/)

# Goal:

Today the goal is to start learning how we can work with big data sets and make them useful for us by trying to solve a number of problems. This tutorial is going to be centered around spinning up a Linux Server and using basic bash commands to get a set of data and manipulate it. After today you should be able to:

Set up a Server

Pull a Set of data

"Look" at the data

Have a plan to be able to manipulate that data. 

# Set up Server:

[https://www.digitalocean.com/?utm_source=TechSNAP&utm_medium=Podcast&utm_campaign=TechSNAP+Podcast](https://www.digitalocean.com/?utm_source=TechSNAP&utm_medium=Podcast&utm_campaign=TechSNAP+Podcast)

Make an account and use the promotion code: snapocean

This code will get you $10 credit you can use.

Click "Create Droplet"

Select "Ubuntu 14.04"

Select $5/month

Select where you want your server to live

Check your email for IP address and password

On Windows Download Putty:

[https://the.earth.li/~sgtatham/putty/latest/x86/putty.exe](https://the.earth.li/~sgtatham/putty/latest/x86/putty.exe)

Open Putty and use the IP address and password as follows:

ssh  root@123.123.123.123

The numbers is your IP address for your server

When prompted enter your password.

Welcome to your server!!!!

# Pull over a data set:

Overview of what can be done with this data is below for inspiration:

[http://toddwschneider.com/posts/analyzing-1-1-billion-nyc-taxi-and-uber-trips-with-a-vengeance/](http://toddwschneider.com/posts/analyzing-1-1-billion-nyc-taxi-and-uber-trips-with-a-vengeance/)

Source of data:

[http://www.nyc.gov/html/tlc/html/about/trip_record_data.shtml](http://www.nyc.gov/html/tlc/html/about/trip_record_data.shtml)

The files [trip_data_3.csv.zip](https://nyctaxitrips.blob.core.windows.net/data/trip_data_3.csv.zip) and [trip_fare_3.csv.zip](https://nyctaxitrips.blob.core.windows.net/data/trip_fare_3.csv.zip) contain travel and fare data for NYC taxicabs in March 2013. A snippet from the files looks like this:

5D7054D121F7AFEF5D67FD93AB1DF05E,3DD058605BB5583F4484CE64D7AC57D9,CMT,1,N,2013-03-01 00:00:09,2013-03-01 00:26:34,4,1584,5.40,-73.986534,40.734962,-73.975365,40.67527
213C1F221955A2823F4AFC8A65A951AC,6AE0CCB3191A4354FDF1AAF377ACFD15,CMT,1,N,2013-03-01 00:00:17,2013-03-01 00:16:04,1,946,3.90,-73.988701,40.73122,-73.95739,40.722301
5EE2C4D3BF57BDB455E74B03B89E43A7,79EFAD78E0A81463C273B1CF7A8D3583,CMT,1,N,2013-03-01 00:00:28,2013-03-01 00:24:28,1,1438,9.40,-73.973839,40.752171,-73.958481,40.653683
3D14197B6B792B6CE2B1D34E916A81AE,434A5F0C3E2405DF2F5450B146EABE6E,CMT,1,N,2013-03-01 00:00:33,2013-03-01 00:12:00,1,687,3.50,-74.003777,40.723763,-73.995758,40.764538
F235E92D0522C9BB25DA3767BE919705,5C8C81864C3CFDB140FF9F6C25E1DBF7,CMT,1,N,2013-03-01 00:00:34,2013-03-01 00:15:23,1,889,7.40,-73.986382,40.749817,-73.94371,40.835423

and this, respectively

D30BED60331C79E3F7ACD05B325ED42F,E0F3F8A7AE1B8DCCE0C0BA42FFBA4610,CMT,2013-03-01 00:05:48,CRD,10.5,0.5,0.5,2.3,0,13.8
D886C7DC412D236CF1797D4B5BC90DDB,9B87622D5FD8A9F31C61FBA3C04DF8AD,CMT,2013-03-01 00:05:55,CRD,52,0,0.5,13.8,16.5,82.8
049DC01A4AC7D474B94393E642561A3B,AE30D3668D5E76CF4C24E2FF6432F17B,CMT,2013-03-01 00:06:02,CRD,12.5,0.5,0.5,1.8,0,15.3
A28B5AE58590D2ADF1D9A709D80B2984,3B681501AFD2BED5E0FF8F075C6E56E5,CMT,2013-03-01 00:06:09,CRD,8.5,0.5,0.5,1.5,0,11
3E5703FEDCBD06883431F9A52AEF6796,1D8EE84A1578FBB1F4AF455225649677,CMT,2013-03-01 00:06:17,CRD,5.5,0.5,0.5,1.62,0,8.12

The headers are contained in the files and are fairly self-explanatory, although some things will have to be inferred from the data. When working with this file, be sure to get rid of "bad" data points. All monetary quantities in these questions are in units of dollars.

# Getting Data:

To pull data you need to use a series of commands to tell your server to ask for the data. First find the link to your data by right clicking the information you are looking for from the source data link above. The link should be something like this:

[https://storage.googleapis.com/tlc-trip-data/2015/yellow_tripdata_2015-12.csv](https://storage.googleapis.com/tlc-trip-data/2015/yellow_tripdata_2015-12.csv)

The above is a link to the December Yellow Cab trip data.

The command to get this data is simply: 

wget https://storage.googleapis.com/tlc-trip-data/2015/yellow_tripdata_2015-12.csv

Your server will start to pull the data, and you will see how quickly this rather large file will be transferred because your server is in a data center. I got 480 Mb/shea which is pretty amazing!

Congrats, you now have a month worth of NYC cab data on your server. Let’s start to look at it!

# Looking at your data:

There are lots of ways to "look" at your data but the best thing to do is learn how pointless the raw data actually is. Even in a program like Excel this vast amount of data would be pointless. Lets use the “nano” program to look at this data:

nano yellow_tripdata_2015-12.csv  

That is lots of number! Get out of nano by using the following key combo:

Ctrl x 

I got rid of the above command because the data was too big!!! You would be waiting too long for the file to open!

Now let’s take a look at what we got since looking at the data was rather trivial. Let’s find the headings of the CSV files using the "head" command. The head command will by default show the top 10 lines:

head yellow_tripdata_2015-12.csv


Notice that the first line is the heading with all the column names. This is important to remember these but we might just want to make a copy:

head -q -n 1 *.csv > output.csv

Now you can simply:

cat output.csv

You can imagine this will begin to get old trying to pull information with the command line language bash. It is however not impossible… [http://stackoverflow.com/questions/1560393/bash-shell-scripting-csv-parsing](http://stackoverflow.com/questions/1560393/bash-shell-scripting-csv-parsing)

[https://www.youtube.com/watch?v=OecFFZpIkDc](https://www.youtube.com/watch?v=OecFFZpIkDc)

# Manipulate Data:

As we saw using bash is not the easiest way to manipulate data. You can imagine that the various other languages you can use with all have pros and cons. Right now I will introduce you to one scripting language to get you started, Python. Python has a great CSV library and can be used in various ways to sort and select parts of data. Working this way will eventually be the basis for working with data from the command line. As you start to put together scripts you will begin to see the power of working with this method. 

[https://www.ocf.berkeley.edu/~dlevitt/2015/12/13/final-project-nyc-taxi-and-uber-data/](https://www.ocf.berkeley.edu/~dlevitt/2015/12/13/final-project-nyc-taxi-and-uber-data/)

It may seem foreign and frustrating working with your hands tied behind your back but as you start to work with the tools (which just happen to be very powerful and free!) you will be able to make scripts that can search and sort as well as you can in Excel. In our original example you see the author used PostgreSQL as a database to store all the data. As you learn more and get more advanced you will find solutions to the problems you create. See below for exactly how that PostgreSQL server was set up!

# Questions to Answer:

What fraction of payments under $5 use a credit card?

What fraction of payments over $50 use a credit card?

What is the mean fare per minute driven?

What is the median of the taxi's fare per mile driven?

What is the 95 percentile of the taxi's average driving speed in miles per hour?

What is the average ratio of the distance between the pickup and dropoff divided by the distance driven?

What is the average tip for rides from JFK?

What is the median March revenue of a taxi driver?

Please provide the script used to generate this result (max 10000 characters).

Languages that can be used:

 C/C++

 Fortran

 IDL

 Java

 Matlab

 Perl

 Python

 R

 Stata

 SQL

 VBA

 Other

# Cheating

[https://github.com/toddwschneider/nyc-taxi-data](https://github.com/toddwschneider/nyc-taxi-data)

Most of the work done in the website using the PostgreSQL method is located on gitHub and can be moved to your server very easily using git… a lesson for another day.


