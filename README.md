__Data Management__
===================

This repository is meant for all code, scripts or useful files for [Data Managment] classes. If you don't know how to use GIT to begin to contribute please see [Contributors]. 

__Lessons__
---------------------------

* [__Lesson 1__](lessons/lesson1.md)
* [__Lesson 2__](lessons/lesson2.md)



[Data Managment]:lessons
[Contributors]: contributors.md